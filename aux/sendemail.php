<?php
// from the form
$fname = trim(strip_tags($_POST['fname']));
$lname = trim(strip_tags($_POST['lname']));
$email = trim(strip_tags($_POST['email']));
$application = htmlentities($_POST['application']);

// set here
$subject = "qr_mumps download request";
$to = 'alfredo.buttari@irit.fr';

$body = <<<HTML
First name: $fname <br/>
Last name: $lname <br/>
email : $email <br/>
Application: $application <br/>

<br/>
<br/>


qr_mumps v 3.0.3 can be downloaded here:<br/>

<a href=http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-3.0.3.tgz>http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-3.0.3.tgz</a>

<br/>
<br/>
<br/>
Note that the old 1.2 version with OpenMP parallelism is still accessible at this address:<br/>


<a href=http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-1.2.tgz>http://buttari.perso.enseeiht.fr/qr_mumps/releases/qr_mumps-1.2.tgz</a>

<br/><br/><br/>

================================================= <br/>

Copyright 2012-2020 CNRS, INPT                                   <br/> 
Copyright 2013-2015 UPS                                          <br/> 
                                                                 <br/> 
qr_mumps is free software: you can redistribute it and/or modify <br/> 
it under the terms of the GNU Lesser General Public License as   <br/> 
published by the Free Software Foundation, either version 3 of   <br/> 
the License, or (at your option) any later version.              <br/> 
                                                                 <br/> 
qr_mumps is distributed in the hope that it will be useful,      <br/> 
but WITHOUT ANY WARRANTY; without even the implied warranty of   <br/> 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    <br/> 
GNU Lesser General Public License for more details.              <br/> 
                                                                 <br/> 
You can find a copy of the GNU Lesser General Public License     <br/> 
in the qr_mumps/doc directory.                                   <br/> 
HTML;

$headers = "From: $email\r\n";
$headers .= "Content-type: text/html\r\n";

if(isset($_POST['url']) && $_POST['url'] == ''){
// send the email
    mail($to, $subject, $body, $headers);
}
// redirect afterwords, if needed
header('Location: thank-you.html');
?>
